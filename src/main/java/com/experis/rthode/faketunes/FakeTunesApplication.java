package com.experis.rthode.faketunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the FakeTunes application.
 */
@SpringBootApplication
public class FakeTunesApplication {

    public static void main(String[] args) {
        SpringApplication.run(FakeTunesApplication.class, args);
    }
}
