package com.experis.rthode.faketunes.controllers;

import com.experis.rthode.faketunes.DAO.artist.ArtistDAO;
import com.experis.rthode.faketunes.DAO.artist.ArtistSQLiteAccessor;
import com.experis.rthode.faketunes.DAO.genre.GenreDAO;
import com.experis.rthode.faketunes.DAO.genre.GenreSQLiteAccessor;
import com.experis.rthode.faketunes.DAO.track.TrackDAO;
import com.experis.rthode.faketunes.DAO.track.TrackSQLiteAccessor;
import com.experis.rthode.faketunes.models.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import java.util.List;

/**
 * Controller for the home page.
 */
@Controller
public class HomeController {
    /**
     * Fetches five random songs, artists and genres and show them in a table. Also has a search bar to search for a specific track.
     *
     * @param model Thymeleaf model
     * @return
     */
    @GetMapping("/")
    public String home(Model model) {
        ArtistDAO artistAccess = new ArtistSQLiteAccessor();
        TrackDAO trackAccess = new TrackSQLiteAccessor();
        GenreDAO genreAccess = new GenreSQLiteAccessor();
        List<Artist> randomArtists = artistAccess.getFiveRandomArtists();
        List<Track> randomTracks = trackAccess.getFiveRandomTracks();
        List<Genre> randomGenres = genreAccess.getFiveRandomGenres();
        model.addAttribute("artists", randomArtists);
        model.addAttribute("tracks", randomTracks);
        model.addAttribute("genres", randomGenres);
        return "home";
    }
}
