package com.experis.rthode.faketunes.controllers;

import com.experis.rthode.faketunes.DAO.customer.CustomerDAO;
import com.experis.rthode.faketunes.DAO.customer.CustomerSQLiteAccessor;
import com.experis.rthode.faketunes.models.CountryCustomers;
import com.experis.rthode.faketunes.models.Customer;
import com.experis.rthode.faketunes.models.CustomerSpendings;
import com.experis.rthode.faketunes.models.FavouriteGenre;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Endpoint for the customer model.
 */
@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    /**
     * Fetches a customer from the database by its id.
     *
     * @param customerId
     * @return
     */
    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomer(@PathVariable int customerId) {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        Customer customer = dataAccess.getCustomerById(customerId);
        if (customer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(customer);
    }

    /**
     * Creates a new customer in the database.
     *
     * @param customer The customer to be inserted and all its information.
     * @return
     */
    @PutMapping("/")
    public ResponseEntity<String> createCustomer(@RequestBody Customer customer) {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        if (!dataAccess.createCustomer(customer)) {
            return ResponseEntity.badRequest().body("There was an error in the provided data.");
        }
        return ResponseEntity.ok().body("The user \"" + customer.firstName + "\" has been created successfully");
    }

    /**
     * Updates an existing customer.
     *
     * @param customer The customer to update with all the updated information.
     * @return
     */
    @PostMapping("/")
    public ResponseEntity<String> updateCustomer(@RequestBody Customer customer) {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        if (!dataAccess.updateExistingCustomer(customer)) {
            return ResponseEntity.badRequest().body("The user with the id " + customer.customerId + " does not exist in the system.");
        }
        return ResponseEntity.ok().body("The user \"" + customer.firstName + "\" has been updated successfully");
    }

    /**
     * Counts the number of customers for each country and sorts them by number of customers in descending order.
     *
     * @return The list with countries and the number of customers in it.
     */
    @GetMapping("/countries/total")
    public ResponseEntity<List<CountryCustomers>> getCustomersForEachCountry() {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        List<CountryCustomers> countries = dataAccess.getNumberOfCustomersInEachCountry();
        if (countries.size() == 0) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(countries);
    }

    /**
     * Gets a list of the customers that spend the most money on buying music.
     *
     * @return The ten highest spenders.
     */
    @GetMapping("/purchases/spendings/top")
    public ResponseEntity<List<CustomerSpendings>> getHighestSpenders() {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        List<CustomerSpendings> bigSpenders = dataAccess.getHighestSpenders();
        if (bigSpenders.size() == 0) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(bigSpenders);
    }

    /**
     * Gets the genre that a specified customer has most purchased of.
     *
     * @param customerId The id of the customer to get favourite genre for.
     * @return A list containing the customer and its favourite genre. If there is a tie, all the top genres are returned.
     */
    @GetMapping("/{customerId}/popular/genre")
    public ResponseEntity<FavouriteGenre> getFavouriteGenre(@PathVariable int customerId) {
        CustomerDAO dataAccess = new CustomerSQLiteAccessor();
        FavouriteGenre favouriteGenres = dataAccess.getCustomersFavouriteGenre(customerId);
        if (favouriteGenres == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(favouriteGenres);
    }
}
