package com.experis.rthode.faketunes.controllers;

import com.experis.rthode.faketunes.DAO.track.TrackDAO;
import com.experis.rthode.faketunes.DAO.track.TrackSQLiteAccessor;
import com.experis.rthode.faketunes.models.FullTrackInformation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Controller for the search result page.
 */
@Controller
public class SearchController {
    /**
     * Fetches songs in the database that match the search query and shows them in a table.
     *
     * @param search query
     * @param model Thymeleaf model for the view
     * @return
     */
    @GetMapping("/search")
    public String greeting(@RequestParam(name = "search") String search, Model model) {
        TrackDAO trackAccess = new TrackSQLiteAccessor();
        List<FullTrackInformation> tracks = trackAccess.getTrackSearchResult(search);
        model.addAttribute("tracks", tracks);
        model.addAttribute("query", search);
        return "searchPage";
    }
}
