package com.experis.rthode.faketunes.models;

/**
 * Contains all the data for a customer entity.
 */
public class Customer {
    public int customerId;
    public String firstName;
    public String lastName;
    public String postalCode;
    public String country;
    public String phone;
    public String email;

    public Customer() {
    }

    public Customer(int customerId, String firstName, String lastName, String postalCode, String country, String phone, String email) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
        this.email = email;
    }
}
