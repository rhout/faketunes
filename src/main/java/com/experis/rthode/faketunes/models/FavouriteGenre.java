package com.experis.rthode.faketunes.models;

import java.util.List;

/**
 * Contains a customer (id, and name) and its most bought genres.
 */
public class FavouriteGenre {
    public int customerId;
    public String firstName;
    public String lastName;
    public List<String> genres;

    public FavouriteGenre(int customerId, String firstName, String lastName, List<String> genres) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.genres = genres;
    }
}
