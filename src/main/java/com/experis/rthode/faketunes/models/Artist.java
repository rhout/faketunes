package com.experis.rthode.faketunes.models;

/**
 * Holds the basic artist information.
 */
public class Artist {
    public int id;
    public String artistName;

    public Artist(int id, String name) {
        this.id = id;
        this.artistName = name;
    }
}
