package com.experis.rthode.faketunes.models;

/**
 * Holds the information of how many customers come from the given country.
 */
public class CountryCustomers {
    public String country;
    public int customers;

    public CountryCustomers(String country, int customers) {
        this.country = country;
        this.customers = customers;
    }
}
