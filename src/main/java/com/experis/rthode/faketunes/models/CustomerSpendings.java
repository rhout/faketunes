package com.experis.rthode.faketunes.models;

/**
 * Contains a customers id, name and how much that customer has spent on music in total.
 */
public class CustomerSpendings {
    public int customerId;
    public String firstName;
    public String lastName;
    public float spendings;

    public CustomerSpendings(int customerId, String firstName, String lastName, float spendings) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.spendings = spendings;
    }
}
