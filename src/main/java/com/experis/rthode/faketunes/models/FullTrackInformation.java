package com.experis.rthode.faketunes.models;

/**
 * Holds track information with extra fields compared to {@link Track}.
 */
public class FullTrackInformation {
    int trackId;
    public String trackName;
    public String artistName;
    public String albumName;
    public String genreName;

    public FullTrackInformation(int trackId, String trackName, String artistName, String albumName, String genreName) {
        this.trackId = trackId;
        this.trackName = trackName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.genreName = genreName;
    }
}
