package com.experis.rthode.faketunes.models;

/**
 * Holds the information for a track.
 */
public class Track {
    public int id;
    public String name;

    public Track(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
