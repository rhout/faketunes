package com.experis.rthode.faketunes.DAO.genre;

import com.experis.rthode.faketunes.DAO.ConnectionHelper;
import com.experis.rthode.faketunes.DAO.SQLStatements;
import com.experis.rthode.faketunes.models.Genre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * SQLite implementation of the genre DAO.
 */
public class GenreSQLiteAccessor implements GenreDAO {
    String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    public List<Genre> getFiveRandomGenres() {
        List<Genre> genres = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(SQLStatements.GET_FIVE_RANDOM_GENRES);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                genres.add(new Genre(
                        resultSet.getInt("GenreId"),
                        resultSet.getString("Name")
                ));
            }
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
            return genres;
        }
    }
}
