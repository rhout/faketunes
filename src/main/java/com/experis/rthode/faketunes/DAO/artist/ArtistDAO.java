package com.experis.rthode.faketunes.DAO.artist;

import com.experis.rthode.faketunes.models.Artist;

import java.util.List;

/**
 * Interface for accessing the track data.
 */
public interface ArtistDAO {
    /**
     * Gets five random artists from the database.
     *
     * @return
     */
    List<Artist> getFiveRandomArtists();
}
