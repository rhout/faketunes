package com.experis.rthode.faketunes.DAO.track;

import com.experis.rthode.faketunes.DAO.ConnectionHelper;
import com.experis.rthode.faketunes.DAO.SQLStatements;
import com.experis.rthode.faketunes.models.FullTrackInformation;
import com.experis.rthode.faketunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * SQLite implementation of the track DAO.
 */
public class TrackSQLiteAccessor implements TrackDAO {
    String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    public List<Track> getFiveRandomTracks() {
        List<Track> tracks = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(SQLStatements.GET_FIVE_RANDOM_TRACKS);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                tracks.add(new Track(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("Name")
                ));
            }
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
            return tracks;
        }
    }

    public List<FullTrackInformation> getTrackSearchResult(String searchQuery) {
        List<FullTrackInformation> tracks = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(SQLStatements.GET_SEARCH_TRACK_RESULT);
            preparedStatement.setString(1, "%" + searchQuery + "%");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                tracks.add(new FullTrackInformation(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("TrackName"),
                        resultSet.getString("ArtistName"),
                        resultSet.getString("AlbumTitle"),
                        resultSet.getString("GenreName")
                ));
            }
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
            return tracks;
        }
    }
}
