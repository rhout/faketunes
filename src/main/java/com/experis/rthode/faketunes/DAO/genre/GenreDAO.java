package com.experis.rthode.faketunes.DAO.genre;

import com.experis.rthode.faketunes.models.Genre;

import java.util.List;

/**
 * Interface for accessing the genre data.
 */
public interface GenreDAO {
    /**
     * Gets five random genres from the database.
     *
     * @return
     */
    List<Genre> getFiveRandomGenres();
}
