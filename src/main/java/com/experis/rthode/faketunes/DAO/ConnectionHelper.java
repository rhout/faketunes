package com.experis.rthode.faketunes.DAO;

/**
 * Contains the connection URL to the database.
 */
public class ConnectionHelper {
    public static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
}
