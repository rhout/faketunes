package com.experis.rthode.faketunes.DAO.customer;

import com.experis.rthode.faketunes.models.*;

import java.util.List;

/**
 * Interface for accessing the customer data.
 */
public interface CustomerDAO {
    Customer getCustomerById(int customerId);

    boolean createCustomer(Customer customer);

    boolean updateExistingCustomer(Customer updatedCustomer);

    /**
     * Fetches a list of countries with the number of customers from that country.
     * The list is sorted according to the number of customers in descending order.
     *
     * @return
     */
    List<CountryCustomers> getNumberOfCustomersInEachCountry();

    /**
     * Get the ten customers that has spent the most money on buying music.
     *
     * @return
     */
    List<CustomerSpendings> getHighestSpenders();

    /**
     * Get the genre(s) that a specified customer has bought the most.
     *
     * @param customerId The id of the customer to get the favourite genre from
     * @return The customers name and the top genre(s)
     */
    FavouriteGenre getCustomersFavouriteGenre(int customerId);

}
