package com.experis.rthode.faketunes.DAO.customer;

import com.experis.rthode.faketunes.DAO.ConnectionHelper;
import com.experis.rthode.faketunes.DAO.SQLStatements;
import com.experis.rthode.faketunes.models.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SQLite implementation of the customer DAO.
 */
public class CustomerSQLiteAccessor implements CustomerDAO {
    String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    public Customer getCustomerById(int customerId) {
        Customer customer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(SQLStatements.GET_CUSTOMER_BY_ID);
            preparedStatement.setInt(1, customerId); // Corresponds to 1st '?' (must match type)
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            if (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Country"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
            return customer;
        }
    }

    public boolean createCustomer(Customer customer) {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established");

            PreparedStatement preparedStatement = conn.prepareStatement(SQLStatements.CREATE_CUSTOMER);
            preparedStatement.setString(1, customer.firstName);
            preparedStatement.setString(2, customer.lastName);
            preparedStatement.setString(3, customer.postalCode);
            preparedStatement.setString(4, customer.country);
            preparedStatement.setString(5, customer.phone);
            preparedStatement.setString(6, customer.email);

            preparedStatement.executeUpdate();

            return true;
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
            return false;
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
        }
    }

    public boolean updateExistingCustomer(Customer updatedCustomer) {
        boolean successfulUpdate = false;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established");

            PreparedStatement preparedStatement = conn.prepareStatement(SQLStatements.UPDATE_CUSTOMER);
            preparedStatement.setString(1, updatedCustomer.firstName);
            preparedStatement.setString(2, updatedCustomer.lastName);
            preparedStatement.setString(3, updatedCustomer.postalCode);
            preparedStatement.setString(4, updatedCustomer.country);
            preparedStatement.setString(5, updatedCustomer.phone);
            preparedStatement.setString(6, updatedCustomer.email);
            preparedStatement.setInt(7, updatedCustomer.customerId);

            successfulUpdate = preparedStatement.executeUpdate() != 0;
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
        }
        return successfulUpdate;
    }

    public List<CountryCustomers> getNumberOfCustomersInEachCountry() {
        List<CountryCustomers> countries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established");

            PreparedStatement preparedStatement = conn.prepareStatement(SQLStatements.GET_CUSTOMERS_PER_COUNTRY);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countries.add(new CountryCustomers(resultSet.getString("Country"), resultSet.getInt("Count")));
            }

        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
        }
        return countries;
    }

    public List<CustomerSpendings> getHighestSpenders() {
        List<CustomerSpendings> highestSpenders = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established");

            PreparedStatement preparedStatement = conn.prepareStatement(SQLStatements.GET_CUSTOMER_WITH_MOST_SPENDINGS);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                highestSpenders.add(new CustomerSpendings(resultSet.getInt("CustomerId"), resultSet.getString("FirstName"), resultSet.getString("LastName"), resultSet.getInt("Sum")));
            }
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
        }
        return highestSpenders;
    }

    public FavouriteGenre getCustomersFavouriteGenre(int customerId) {
        FavouriteGenre favouriteGenres = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection established");
            PreparedStatement preparedStatement = conn.prepareStatement(SQLStatements.GET_CUSTOMERS_FAVOURITE_GENRE);
            preparedStatement.setInt(1, customerId);
            preparedStatement.setInt(2, customerId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                favouriteGenres = new FavouriteGenre(resultSet.getInt("CustomerId"), resultSet.getString("FirstName"), resultSet.getString("LastName"), new ArrayList<>());

                do {
                    favouriteGenres.genres.add(resultSet.getString("Genre"));
                } while (resultSet.next());
            }


        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
        }
        return favouriteGenres;
    }
}
