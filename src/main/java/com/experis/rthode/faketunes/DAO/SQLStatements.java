package com.experis.rthode.faketunes.DAO;

/**
 * SQL queries used in the application.
 */
public class SQLStatements {
    // API queries
    public static final String GET_CUSTOMER_BY_ID = "SELECT CustomerId,FirstName,LastName,PostalCode,Country,Phone,Email FROM Customer WHERE CustomerId = ?";
    public static final String CREATE_CUSTOMER = "INSERT INTO Customer (FirstName,LastName,PostalCode,Country,Phone,Email) VALUES (?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_CUSTOMER = "UPDATE Customer SET FirstName = ?, LastName = ?, PostalCode = ?, Country = ?, Phone = ?, Email = ? WHERE CustomerId = ?";
    public static final String GET_CUSTOMERS_PER_COUNTRY = "SELECT Country, COUNT(*) AS Count FROM Customer GROUP BY Country ORDER BY Count DESC ";
    public static final String GET_CUSTOMER_WITH_MOST_SPENDINGS = "SELECT Invoice.CustomerId, Customer.FirstName, Customer.LastName, SUM(Total) AS Sum FROM Invoice INNER JOIN Customer on Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.CustomerId ORDER BY Sum DESC LIMIT 10";
    public static final String GET_CUSTOMERS_FAVOURITE_GENRE = "SELECT CustomerId, FirstName, LastName, Name AS Genre FROM (SELECT Customer.CustomerId, Customer.FirstName, LastName, G.Name, COUNT(G.Name) AS Number FROM Customer LEFT JOIN Invoice I on Customer.CustomerId = I.CustomerId LEFT JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId LEFT JOIN Track T on T.TrackId = IL.TrackId LEFT JOIN Genre G on G.GenreId = T.GenreId WHERE Customer.CustomerId = ? GROUP BY G.Name ORDER BY Number DESC) WHERE Number = (SELECT MAX(Number) FROM (SELECT G.Name, COUNT(G.Name) AS Number FROM Customer LEFT JOIN Invoice I on Customer.CustomerId = I.CustomerId LEFT JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId LEFT JOIN Track T on T.TrackId = IL.TrackId LEFT JOIN Genre G on G.GenreId = T.GenreId WHERE Customer.CustomerId = ? GROUP BY G.Name ORDER BY Number DESC))";

    // View queries
    public static final String GET_FIVE_RANDOM_ARTISTS = "SELECT ArtistId, Name from Artist ORDER BY RANDOM() LIMIT 5";
    public static final String GET_FIVE_RANDOM_TRACKS = "SELECT TrackId, Name from Track ORDER BY RANDOM() LIMIT 5";
    public static final String GET_FIVE_RANDOM_GENRES = "SELECT genreId, Name from Genre ORDER BY RANDOM() LIMIT 5";
    public static final String GET_SEARCH_TRACK_RESULT = "SELECT TrackId, Track.Name AS TrackName, A2.Name AS ArtistName, G.Name AS GenreName, Title AS AlbumTitle FROM Track INNER JOIN Album A on A.AlbumId = Track.AlbumId INNER JOIN Artist A2 on A2.ArtistId = A.ArtistId INNER JOIN Genre G on Track.GenreId = G.GenreId WHERE Track.Name LIKE ?";
}
