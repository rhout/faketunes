package com.experis.rthode.faketunes.DAO.artist;

import com.experis.rthode.faketunes.DAO.ConnectionHelper;
import com.experis.rthode.faketunes.DAO.SQLStatements;
import com.experis.rthode.faketunes.models.Artist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * SQLite implementation of the artist DAO.
 */
public class ArtistSQLiteAccessor implements ArtistDAO {
    String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    public List<Artist> getFiveRandomArtists() {
        List<Artist> artists = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(SQLStatements.GET_FIVE_RANDOM_ARTISTS);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                artists.add(new Artist(
                        resultSet.getInt("ArtistId"),
                        resultSet.getString("Name")
                ));
            }
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception e) {
                System.out.println("Something went wrong while closing connection.");
                e.printStackTrace();
            }
            return artists;
        }
    }
}
