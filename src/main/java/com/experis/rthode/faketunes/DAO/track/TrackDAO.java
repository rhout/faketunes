package com.experis.rthode.faketunes.DAO.track;

import com.experis.rthode.faketunes.models.FullTrackInformation;
import com.experis.rthode.faketunes.models.Track;

import java.util.List;

/**
 * Interface for accessing the track data.
 */
public interface TrackDAO {
    /**
     * Gets five random tracks from the database.
     *
     * @return
     */
    List<Track> getFiveRandomTracks();

    /**
     * Gets a list of detailed track information (track name, artist, album and genre) based on a track name search.
     *
     * @param searchQuery The title search query
     * @return
     */
    List<FullTrackInformation> getTrackSearchResult(String searchQuery);
}
