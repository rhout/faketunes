# 	:musical_note: FakeTunes - the new great music service :musical_note:

A simple application that uses SQLite and Java Spring (with Thymeleaf) with:
* A small web application that shows random data from the database and supports a simple search
* A REST endpoint with some CRUD-operations and some data collection queries

Link to the deployed app: https://faketunes.herokuapp.com/ 

The [Postman collection](./src/test/postman_tests.json) is placed in the test folder.