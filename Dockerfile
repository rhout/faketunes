FROM openjdk:15
ADD build/libs/faketunes-0.0.1.jar faketunes.jar
ENTRYPOINT [ "java", "-jar", "faketunes.jar" ]